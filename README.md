Cloner l'application:
```
 git clone https://gitlab.com/c.b/ppeassistance.git
```
Mise en place du fichier .env
```
cp .env.example .env
php artisan key:generate
```

Créer une base de données et un utilisateur dans le SGBD puis compléter le fichier .env avec les informations nécessaire.
Mettre a jour composer :
```
composer update
```
Remplire la bdd avec les migrations et les seeders:
```
php artisan migrate:fresh --seed
```

Pour commencer à utilisé l'application :

```
php artisan serv
```

Vous pouvez maintenant accèder au site web sur un navigateur à l'adresse : http://localhost:8000/

Voici les comptes pour se connecter :

**Salarié**
```
login : visiteur@gsb.fr
mdp   : 12345678
```
**Operateur**
```
login : operateur@gsb.fr
mdp   : 12345678
```
**Technicien**
```
login : technicien@gsb.fr
mdp   : 12345678
```
**Administrateur**
```
login : administrateur@gsb.fr
mdp   : 12345678
```
