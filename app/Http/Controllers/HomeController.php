<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //dd(auth()->user()->name);

        $tickets = DB::table('tickets')->where('user_id', Auth::id())->where('status', '>', 0)->orderBy('postedAt', 'desc')->get();
        $nbticket = DB::table('tickets')->where('status', '>', 0)->count();


        return view('home', ["tickets"=>$tickets, "nbticket"=>$nbticket]);

    }
}
