<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        if($this->role_id == 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isOperator()
    {
        if($this->role_id == 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function isTechnician()
    {
        if($this->role_id == 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function hasRole($role)
    {
        $r = DB::table('role')
            ->select('name')
            ->where('id', '=', $this->role_id)
            ->get();

        if($r[0]->name == $role){
            return true;
        }else{
            return false;
        }

    }


}
