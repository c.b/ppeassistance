<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_response', function (Blueprint $table) {
            $table->bigint('response_id');
            $table->bigint('ticket_id');
            $table->bigint('user_id');
            $table->text('message');
            $table->timestamp('postedAt');
            $table->timestamp('last_updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_response');
    }
}
