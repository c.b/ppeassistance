<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'john',
            'email' => 'visiteur@gsb.fr',
            'password' => bcrypt('12345678'),
            'role_id' => '1',
        ]);
        User::create([
            'name' => 'jason',
            'email' => 'operateur@gsb.fr',
            'password' => bcrypt('12345678'),
            'role_id' => '2',
        ]);
        User::create([
            'name' => 'steven',
            'email' => 'administrateur@gsb.fr',
            'password' => bcrypt('12345678'),
            'role_id' => '3',
        ]);
        User::create([
            'name' => 'john',
            'email' => 'technicien@gsb.fr',
            'password' => bcrypt('12345678'),
            'role_id' => '4',
        ]);

    }
}
