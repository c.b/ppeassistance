@extends('layouts.app')
@section('content')
    <h2>Liste des categories <a data-bs-toggle="modal" data-bs-target="#modal_add" style="cursor: pointer;">
            <span data-feather="plus-circle"></span>
        </a>
    </h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($categories as $c)
                <tr>
                    <td>{{ $c->cat_libelle }}</td>
                    <td>
                        <form action="{{ route('adm_cat_del', $c->cat_id) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer"><span data-feather="x-square"></span></button>

                            <a data-bs-toggle="modal" data-bs-target="#modal_{{ $c->cat_id }}" style="cursor: pointer;" class="btn btn-primary">
                                <span data-feather="edit"></span>
                            </a>

                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    @foreach ($categories as $c)
    <!-- Modal -->
    <div class="modal fade" id="modal_{{ $c->cat_id }}" tabindex="-1" aria-labelledby="modal_assigneLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('adm_cat_edit', $c->cat_id) }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_assigneLabel">Modifier une catégorie</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="cat_libelle" value="{{ $c->cat_libelle }}" class="form form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach


    <!-- Modal -->
    <div class="modal fade" id="modal_add" tabindex="-1" aria-labelledby="modal_assigneLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('adm_cat_add') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_assigneLabel">Ajouter une catégorie</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="cat_libelle" class="form form-control">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop
