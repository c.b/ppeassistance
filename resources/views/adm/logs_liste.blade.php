@extends('layouts.app')
@section('content')


    <div class="card">
        <div class="card-header">
            <h2>Recherche</h2>
        </div>
        <div class="card-body">
            <form action="" method="GET">
                <div class="form-group row">
                    <label for="priorite" class="col-sm-2 col-form-label">Ticket ID</label>
                    <div class="col-sm-10">
                        <input type="number" name="ticket_id" class="form-control">
                        <br>
                        <input type="submit" class="btn btn-primary" value="Valider">
                    </div>
                </div>

            </form>
        </div>
    </div>


    <h2>Chronologie</h2>
<hr/>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Auteur</th>
                <th>Detail</th>
                <th>Ticket ID</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($logs as $l)
                <tr>
                    <td>{{ $l->name }}</td>
                    <td>
                        {!! $l->detail !!}
                        @if($l->ticket_id != NULL)
                            <a href="{{ route('ticketDetail', $l->ticket_id) }}">ticket {{ $l->ticket_id }}</a>
                        @endif
                    </td>
                    <td>
                        {{ $l->ticket_id }}
                    </td>
                    <td>{{ date('d/m/y à H:i:s', strtotime($l->postedAt)) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {{ $logs->links('layouts.paginator') }}


@stop
