@extends('layouts.app')
@section('content')
    <h2>Ticket en attente d'assignement</h2>
    <hr/>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Sujet</th>
                <th>Priorité</th>
                <th>Ajouté le</th>
                <th>Auteur</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($tickets as $t)
                <tr>
                    <td>{{ $t->id }}</td>
                    <td><a href="{{ route('ticketDetail', $t->id) }}" style="color:{{$t->color}};">{{ $t->objet }}</a></td>
                    <td>
                        @if($t->priority == 1) <span class="badge badge-secondary">Basse</span> @endif
                        @if($t->priority == 2) <span class="badge badge-info">Normal</span> @endif
                        @if($t->priority == 3) <span class="badge badge-warning">Majeur</span> @endif
                        @if($t->priority == 4) <span class="badge badge-danger">Critique</span> @endif
                    </td>
                    <td>{{ date('d/m/y à H:i:s', strtotime($t->postedAt)) }}</td>
                    <td>{{ $t->name }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
