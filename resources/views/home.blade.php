@extends('layouts.app')
@section('content')
    <h2>Mes tickets en cours</h2><br>
    <p>
        Nombre de tickets en cours : <b>{{ $nbticket }}</b>
    </p>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Sujet</th>
                <th>Ajouté le</th>
                <th>Mis à jour le</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($tickets as $t)
                <tr>
                    <td>{{ $t->id }}</td>
                    <td><a href="{{ route('ticketDetail', $t->id) }}" style="color:{{$t->color}};">{{ $t->objet }}</a></td>
                    <td>{{ $t->postedAt }}</td>
                    <td>{{ $t->updatedAt }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
