
<form action="" method="POST">
    @csrf

    <table class="table table-success table-striped">
        <tr>
            <th>ID</th>
            <th>Categorie</th>
            <th>Etat</th>
            <th>Date d'ajout</th>

        </tr>
        <tr>
            <td>{{ $ticket->id }}</td>

            <td>
                <select name="cat_id" id="" class="form-control">
                    @foreach ($categories as $cat)
                        <option value="{{ $cat->cat_id }}"
                                @if($ticket->cat_id == $cat->cat_id)
                                selected
                            @endif
                        >{{ $cat->cat_libelle }}</option>
                    @endforeach
                </select>
            </td>

            <td>

                <select name="status" id="status" class="form form-control">
                    <option value="0" @if($ticket->status == 0) selected @endif>Résolu</option>
                    @if($ticket->status != 1)
                        <option value="1" @if($ticket->status == 1) selected @endif>En attente</option>
                    @endif
                    <option value="2" @if($ticket->status == 2) selected @endif>Assigné</option>
                </select>


            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->postedAt)) }}</td>

        </tr>
        <tr>
            <th>Severite</th>
            <th colspan="2">Priorite</th>
            <th>Mis à jour le</th>
        </tr>
        <tr>
            <td>
                <select name="lvl" id="lvl" class="form form-control">
                    <option value="1" @if($ticket->lvl == 1) selected @endif>Mineur</option>
                    <option value="2" @if($ticket->lvl == 2) selected @endif>Majeur</option>
                    <option value="3" @if($ticket->lvl == 3) selected @endif>Critique</option>
                </select>
            </td>
            <td>
                <select name="priority" id="priority" class="form form-control">
                    <option value="1" @if($ticket->priority == 1) selected @endif>Basse</option>
                    <option value="2" @if($ticket->priority == 2) selected @endif>Normal</option>
                    <option value="3" @if($ticket->priority == 3) selected @endif>Majeur</option>
                    <option value="4" @if($ticket->priority == 4) selected @endif>Critique</option>
                </select>
            </td>
            <td>
                <!-- Button trigger modal -->

            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->updatedAt)) }}</td>
        </tr>
    </table>
    <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
    <button type="submit" class="btn btn-dark">Enregistrer</button>
</form>

<b>Assigné à <a data-bs-toggle="modal" data-bs-target="#modal_assigne" style="cursor: pointer;">
        <span data-feather="plus-circle"></span>
    </a> : </b>
@foreach ($technician as $t)
    <form action="{{ route('Adm_ticket_del_technician') }}" method="POST" class="float-right">
        {{ $t->name }}
        @csrf
        <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
        <input type="hidden" name="user_id" value="{{ $t->id }}">
        <button type="submit" class="btn btn-link" style="color:red;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer"><span data-feather="x-square"></span></button>
    </form>
@endforeach
