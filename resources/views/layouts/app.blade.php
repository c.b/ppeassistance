<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Dashboard Template for Bootstrap</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">
    <style>
        body{
            font-size: 16px;
        }
        .title{
            padding: 10px;background: #DFDFDF;margin-top: 15px;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Helpdesk</a>

    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

<!--
    <form action="{{ route('search') }}" method="POST" role="search">
        @csrf
            <input class="form-control form-control-dark w-100" type="text" placeholder="Rechercher un ticket" aria-label="Search" name="words"">

            <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </form>
-->

    <ul class="navbar-nav px-3">


        <ul class="navbar-nav px-3">

            <li class="nav-item text-nowrap">

                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Déconnexion
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>

        </ul>

    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="position-sticky pt-3">
                <ul class="nav flex-column">

                    <li class="nav-item">
                        <a aria-current="page" class="nav-link" href="/">
                            <span data-feather="home"></span>
                            GSB ACCUEIL
                        </a>
                    </li>

                    <li class="nav-item">
                        <a @if (Request::fullUrl() == route('home')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('home') }}">
                            <span data-feather="home"></span>
                            Mes tickets
                        </a>
                    </li>
                    <li class="nav-item">
                        <a @if (Request::fullUrl() == route('ticketNew')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('ticketNew') }}">
                            <span data-feather="file-text"></span>
                            Nouveau ticket
                        </a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link" href="">
                            <span data-feather="users"></span>
                            Parametres
                        </a>
                    </li>
                    -->
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Administration</span>
                </h6>
                <ul class="nav flex-column mb-2">

                        <li class="nav-item">
                            <a @if (Request::fullUrl() == route('Adm_ticket_a_assigne')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('Adm_ticket_a_assigne') }}">
                                <span data-feather="file-text"></span>
                                Liste des tickets en attente
                            </a>
                        </li>

                    <li class="nav-item">
                        <a @if (Request::fullUrl() == route('adm_logs_liste')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('adm_logs_liste') }}">
                            <span data-feather="file-text"></span>
                            Chronologie
                        </a>
                    </li>

                            <li class="nav-item">
                                <a @if (Request::fullUrl() == route('adm_cat_list')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('adm_cat_list') }}">
                                    <span data-feather="file-text"></span>
                                    Gestion des categories
                                </a>
                            </li>

                    <li class="nav-item">
                        <a @if (Request::fullUrl() == route('Adm_ticket_liste')) class="nav-link active" aria-current="page" @else class="nav-link" @endif href="{{ route('Adm_ticket_liste') }}">
                            <span data-feather="file-text"></span>
                            Mes tickets
                        </a>
                    </li>

                </ul>
            </div>
        </nav>


        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <br/>

            @include('layouts.message')


            @yield('content')
        </main>
        <footer class="footer">
            <div class="container">
                <span class="text-muted">Date de dernière modification du site :

                @php
                    setlocale(LC_ALL,'french');
                echo date("l j F Y à H:i", getlastmod());

                @endphp
                </span>
            </div>
        </footer>

    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="/js/bootstrap.bundle.min.js" integrity="" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script><script src="https://getbootstrap.com/docs/5.0/examples/dashboard/dashboard.js"></script>


</body>
</html>


