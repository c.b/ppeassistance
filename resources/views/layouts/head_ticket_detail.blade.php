<form action="" method="POST">
    @csrf

    <table class="table table-success table-striped">
        <tr>
            <th>ID</th>
            <th>Categorie</th>
            <th>Etat</th>
            <th>Date d'ajout</th>

        </tr>
        <tr>
            <td>{{ $ticket->id }}</td>

            <td>
                {{ $ticket->cat_libelle }}
            </td>

            <td>

                    @if($ticket->status == 0) Résolu @endif
                    @if($ticket->status == 1) En attente @endif
                     @if($ticket->status == 2) Assigné @endif

            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->postedAt)) }}</td>

        </tr>
        <tr>
            <th>Severite</th>
            <th>Priorite</th>
            <th>Assigné à</th>
            <th>Mis à jour le</th>
        </tr>
        <tr>
            <td>
                @if($ticket->lvl == 1) Mineur @endif
                @if($ticket->lvl == 2) Majeur @endif
                @if($ticket->lvl == 3) Critique @endif
            </td>
            <td>
                @if($ticket->priority == 1) Basse @endif
                @if($ticket->priority == 2) Normal @endif
                @if($ticket->priority == 3) Majeur @endif
                @if($ticket->priority == 4) Critique @endif
            </td>
            <td>
                @foreach ($technician as $t)
                    <b>{{ $t->name }}</b>
                @endforeach

            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->updatedAt)) }}</td>
        </tr>
    </table>
    <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
    <button type="submit" class="btn btn-dark">Enregistrer</button>
</form>
