@extends('layouts.app')
@section('content')
    <style>
        .form-group{
            margin-bottom: 20px;
        }
    </style>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>tinymce.init({selector:'textarea'});</script>

    <table class="table table-success table-striped">
        <tr>
            <th>ID</th>
            <th>Categorie</th>
            <th>Etat</th>
            <th>Date d'ajout</th>

        </tr>
        <tr>
            <td>{{ $ticket->id }}</td>

            <td>{{ $ticket->cat_libelle }}
            </td>

            <td>
                @if($ticket->status == 0) Résolu @endif

                @if($ticket->status == 1) En attente @endif

                @if($ticket->status == 2) Assigné @endif
            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->postedAt)) }}</td>

        </tr>
        <tr>
            <th>Severite</th>
            <th>Priorite</th>
            <th>Utilisateurs assignés</th>
            <th>Mis à jour le</th>
        </tr>
        <tr>
            <td>
                @if($ticket->lvl == 1) Mineur @endif
                @if($ticket->lvl == 2) Majeur @endif
                @if($ticket->lvl == 3) Critique @endif
            </td>
            <td>
                @if($ticket->priority == 1) Basse @endif
                @if($ticket->priority == 2) Normal @endif
                @if($ticket->priority == 3) Majeur @endif
                @if($ticket->priority == 4) Critique @endif
            </td>
            <td>
                <!-- Button trigger modal -->

                @foreach ($technician as $t)
                    {{ $t->name }} ;
                @endforeach

            </td>
            <td>{{ date('d/m/y à H:i:s', strtotime($ticket->updatedAt)) }}</td>
        </tr>
    </table>
    @if(Auth::user()->isAdmin() OR Auth::user()->hasRole('operator'))
        <a href="{{ route('ticketEdit', $ticket->id) }}" class="btn btn-dark" style="float:left;">Modifier ce ticket</a>
    @endif
    <form action="{{ route('Adm_ticket_solved', $ticket->id) }}" method="POST">
        @csrf
        <button type="submit" class="btn btn-success" style="float:left;margin-left: 15px;">Problème résolu</button>
    </form>

    <div style="clear:both;"></div>

    <div class="card">
        <div class="card-header">
            <h2>{{ $ticket->objet }}</h2>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-1" style="border-right: 2px solid lightgrey;">
                    <b>{{ $ticket_author->name }}</b><br>

                </div>
                <div class="col-11">
                    <small>Le {{ date('d/m/y à H:i:s', strtotime($ticket->postedAt)) }}</small><br>
                    {!!  $ticket->description !!}<br>
                </div>
            </div>


        </div>
    </div>


    <div class="card-header">
        <h2><b>{{ $ticket_responses_count }}</b> @if($ticket_responses_count > 1) réponses @else réponse @endif </h2>
    </div>

    @foreach ($ticket_responses as $t)

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-1" style="border-right: 2px solid lightgrey;">
                        <b>{{ $t->name }}</b><br>
                        <small>{{ $t->description }}</small>
                    </div>
                    <div class="col-11">
                        <small>Le {{ date('d/m/y à H:i:s', strtotime($t->postedAt)) }}</small><br>

                        {!! $t->message !!}

                        @if ($t->user_id == Auth::id())
                            <div class="float-end">
                                <form action="{{ route('deleteAnswer', $t->response_id) }}">
                                    @csrf
                                    <input type="hidden" name="response_id" value="{{ $t->response_id }}">
                                    <input type="hidden" name="ticket_id" value="{{ $t->ticket_id }}">
                                    <button type="submit" class="btn btn-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer"><span data-feather="x-square"></span></button>
                                    <a href="{{ route('editAnswer', $t->response_id) }}" class="btn btn-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Modifier"><span data-feather="edit"></span></a>

                                </form>
                            </div>
                        @endif
                    </div>
                </div>


            </div>
        </div>

    @endforeach

    {{ $ticket_responses->links('layouts.paginator') }}

    @if($ticket->status != 0)

        <form action="{{ route('postAnswer') }}" method="POST">

            <div class="card">
                <div class="card-header">
                    <h2>Poster une réponse</h2>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea id="desc" class="form-control" name="desc"></textarea>
                    </div>
                    @csrf
                    <input type="hidden" name="ticket_id" value="{{ $ticket->id }}" />
                    <div class="form-group">
                        <button class="btn btn-primary">Soumettre la réponse</button>
                    </div>
                </div>
            </div>



        </form>
    @endif


    <!-- Modal -->
    <div class="modal fade" id="modal_assigne" tabindex="-1" aria-labelledby="modal_assigneLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('Adm_ticket_add_technician') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_assigneLabel">Assigner une personne à ce ticket</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <select name="user_id" id="user_id" class="form-control">
                            @foreach ($users as $t)
                                {{ $t->name }}
                                <option value="{{ $t->id }}"> {{ $t->name }} </option>
                            @endforeach
                        </select>
                        <input type="hidden" value="{{ $ticket->id }}" name="ticket_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop
