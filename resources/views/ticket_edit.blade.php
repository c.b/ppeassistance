@extends('layouts.app')
@section('content')
    <style>
        .form-group{
            margin-bottom: 20px;
        }
    </style>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>tinymce.init({selector:'textarea'});</script>

    <small><a href="{{ route('ticketDetail', $ticket->id) }}">&larr; retour</a></small>



    <div class="card">
        <div class="card-header">
            <h2>Modifier un ticket</h2>
        </div>
        <div class="card-body">

    <form action="{{ route('ticketDoEdit', $ticket->id) }}" method="POST">
        @csrf
        <div class="form-group row">
            <label for="severite" class="col-sm-2 col-form-label">Sévérité</label>
            <div class="col-sm-10">
                <select name="severite" id="lvl" class="form form-control">
                    <option value="1" @if($ticket->lvl == 1) selected @endif>Mineur</option>
                    <option value="2" @if($ticket->lvl == 2) selected @endif>Majeur</option>
                    <option value="3" @if($ticket->lvl == 3) selected @endif>Critique</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="priorite" class="col-sm-2 col-form-label">Priorité</label>
            <div class="col-sm-10">
                <select name="priorite" id="priorite" class="form form-control">
                    <option value="1" @if($ticket->priority == 1) selected @endif>Basse</option>
                    <option value="2" @if($ticket->priority == 2) selected @endif>Normal</option>
                    <option value="3" @if($ticket->priority == 3) selected @endif>Majeur</option>
                    <option value="4" @if($ticket->priority == 4) selected @endif>Critique</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="priorite" class="col-sm-2 col-form-label">Catégorie</label>

            <div class="col-sm-10">
                <select name="cat_id" id="" class="form-control">
                    @foreach ($categories as $cat)
                        <option value="{{ $cat->cat_id }}"
                                @if($ticket->cat_id == $cat->cat_id)
                                selected
                            @endif
                        >{{ $cat->cat_libelle }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="color" class="col-sm-2 col-form-label">Couleur</label>

            <div class="col-sm-10">
                <select name="color" id="" class="form-control">
                    <option value="0">Choisir</option>
                    <option value="blue"
                            @if($ticket->color == 'blue')
                            selected
                        @endif>Bleu</option>
                    <option value="red" @if($ticket->color == 'red')
                    selected
                        @endif>Rouge</option>
                    <option value="orange" @if($ticket->color == 'orange')
                    selected
                        @endif>Orange</option>
                    <option value="pink" @if($ticket->color == 'pink')
                    selected
                        @endif>Rose</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="objet" class="col-sm-2 col-form-label">Objet</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="objet" name="objet" value="{{ $ticket->objet }}">
            </div>
        </div>


        <div class="form-group row">
            <label for="desc" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea id="desc" class="form-control" name="desc">
                    {!!  $ticket->description !!}
                </textarea>

                <br>
                <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                <input type="submit" class="btn btn-primary" value="Soumettre les modifications" />
            </div>


        </div>

    </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2>Utilisateurs assigné  :             <a data-bs-toggle="modal" data-bs-target="#modal_assigne" style="cursor: pointer;" class="btn btn-primary">
                    Ajouter un membre
                </a></h2>
        </div>
        <div class="card-body">


        @foreach ($technician as $t)
            <form action="{{ route('Adm_ticket_del_technician') }}" method="POST" class="float-right">
                {{ $t->name }}
                @csrf
                <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                <input type="hidden" name="user_id" value="{{ $t->id }}">
                <button type="submit" class="btn btn-link" style="color:red;" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Supprimer"><span data-feather="x-square"></span></button>
            </form>
        @endforeach
    </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal_assigne" tabindex="-1" aria-labelledby="modal_assigneLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('Adm_ticket_add_technician') }}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_assigneLabel">Assigner une personne à ce ticket</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <select name="user_id" id="user_id" class="form-control">
                            @foreach ($users as $t)
                                {{ $t->name }}
                                <option value="{{ $t->id }}"> {{ $t->name }} </option>
                            @endforeach
                        </select>
                        <input type="hidden" value="{{ $ticket->id }}" name="ticket_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop
