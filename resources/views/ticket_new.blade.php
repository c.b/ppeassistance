@extends('layouts.app')
@section('content')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea'});</script>



    <div class="card">
        <div class="card-header">
            <h2>Nouveau ticket d'incident</h2>
        </div>
        <div class="card-body">
    <form action="{{ route('ticketNew') }}" method="POST">
        @csrf
        <div class="form-group row">
            <label for="severite" class="col-sm-2 col-form-label">Sévérité</label>
            <div class="col-sm-10">
                <select id="severite" name="severite" class="form-control col-sm-10">
                    <option selected>Choisir</option>
                    <option value="1">Mineur</option>
                    <option value="2">Majeur</option>
                    <option value="3">Critique</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="priorite" class="col-sm-2 col-form-label">Priorité</label>
            <div class="col-sm-10">
                <select id="priorite" name="priorite" class="form-control">
                    <option selected>Choisir</option>
                    <option value="1">Basse</option>
                    <option value="2">Normale</option>
                    <option value="3">Majeur</option>
                    <option value="4">Critique</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="priorite" class="col-sm-2 col-form-label">Catégorie</label>

            <div class="col-sm-10">
                <select name="cat_id" id="" class="form-control">
                    <option value="0">Choisir</option>
                    @foreach ($categories as $cat)
                        <option value="{{ $cat->cat_id }}">{{ $cat->cat_libelle }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="color" class="col-sm-2 col-form-label">Couleur</label>

            <div class="col-sm-10">
                <select name="color" id="" class="form-control">
                    <option value="0">Choisir</option>
                    <option value="blue">Bleu</option>
                    <option value="red">Rouge</option>
                    <option value="orange">Orange</option>
                    <option value="pink">Rose</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="objet" class="col-sm-2 col-form-label">Objet</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="objet" name="objet" placeholder="">
            </div>
        </div>


        <div class="form-group row">
            <label for="desc" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <textarea id="desc" class="form-control" name="desc">

                </textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="form-group col-sm-2"></div>
            <div class="form-group col-sm-10">
                <br>
                <input type="submit" class="btn btn-primary" value="Soummetre l'anomalie" />
            </div>
        </div>

    </form>
        </div>
    </div>

@stop
