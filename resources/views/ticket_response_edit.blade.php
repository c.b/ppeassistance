@extends('layouts.app')
@section('content')
    <style>
        .form-group{
            margin-bottom: 20px;
        }
    </style>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea'});</script>

    <small><a href="{{ route('ticketDetail', $ticket_response->ticket_id) }}">&larr; retour</a></small>



    <form action="{{ route('DoEditAnswer', $ticket_response->response_id) }}" method="POST">
        @csrf
        <div class="title">
            <h2>Modification du message</h2>
        </div>
        <div class="form-group">
            <textarea name="message" id="" cols="30" rows="10">
                {{ $ticket_response->message }}
            </textarea>
        </div>

        <div class="form-group">
            <input type="hidden" name="ticket_id" value="{{ $ticket_response->ticket_id }}">
            <input type="hidden" name="response_id" value="{{ $ticket_response->response_id }}">

            <input type="submit" class="btn btn-primary" value="Modifier" />
        </div>

    </form>


@stop
