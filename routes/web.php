<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketController;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/tickets/new', [App\Http\Controllers\TicketController::class, 'new'])->name('ticketNew');
Route::post('/tickets/new', [App\Http\Controllers\TicketController::class, 'postNew'])->name('ticketPostNew');

Route::get('/tickets/detail/{id}', [App\Http\Controllers\TicketController::class, 'detail'])->name('ticketDetail');
Route::get('tickets/edit/{id}', [App\Http\Controllers\TicketController::class, 'detail_edit'])->name('ticketEdit');
Route::post('/tickets/edit/{id}', [App\Http\Controllers\TicketController::class, 'DoEditTicket'])->name('ticketDoEdit');


// réponse tickets
Route::post('/tickets/postAnswer', [App\Http\Controllers\TicketResponseController::class, 'postAnswer'])->name('postAnswer');
Route::get('/tickets/deleteAnswer/{id}', [App\Http\Controllers\TicketResponseController::class, 'deleteAnswer'])->name('deleteAnswer');

Route::get('/tickets/editAnswer/{id}', [App\Http\Controllers\TicketResponseController::class, 'editAnswer'])->name('editAnswer');
Route::post('/tickets/editAnswer/{id}', [App\Http\Controllers\TicketResponseController::class, 'DoEditAnswer'])->name('DoEditAnswer');

Route::post('/search', [App\Http\Controllers\TicketController::class, 'Search'])->name('search');



/********************
 *      ADMIN       *
 ********************/
Route::group(['middleware' => 'auth', 'middleware' => 'roles:Technicien,operator,admin'], function () {
    Route::prefix('admin/')->group(function () {

        // GET
        Route::get('tickets/liste_assigne', [App\Http\Controllers\ADMTicketController::class, 'liste_attente'])->name('Adm_ticket_a_assigne');
        Route::get('tickets/liste', [App\Http\Controllers\ADMTicketController::class, 'liste'])->name('Adm_ticket_liste');
        Route::post('tickets/solved/{id}', [App\Http\Controllers\ADMTicketController::class, 'solved'])->name('Adm_ticket_solved');

        // POST
        Route::post('tickets/detail/{id}', [App\Http\Controllers\ADMTicketController::class, 'detail_edit'])->name('Adm_ticket_edit');
        Route::post('tickets/AddTechnician', [App\Http\Controllers\ADMTicketController::class, 'AddTechnician'])->name('Adm_ticket_add_technician');
        Route::post('tickets/DelTechnician', [App\Http\Controllers\ADMTicketController::class, 'DelTechnician'])->name('Adm_ticket_del_technician');
        Route::post('tickets/edit/{id}', [App\Http\Controllers\TicketController::class, 'postEdit'])->name('postEdit');

        Route::get('chronologie/liste', [App\Http\Controllers\LogsController::class, 'liste'])->name('adm_logs_liste');

        Route::get('categories/liste', [App\Http\Controllers\ADMCategories::class, 'liste'])->name('adm_cat_list');
        Route::post('categories/del/{id}/', [App\Http\Controllers\ADMCategories::class, 'delete'])->name('adm_cat_del');
        Route::post('categories/add', [App\Http\Controllers\ADMCategories::class, 'add'])->name('adm_cat_add');
        Route::post('categories/edit/{id}', [App\Http\Controllers\ADMCategories::class, 'edit'])->name('adm_cat_edit');

    });
});


